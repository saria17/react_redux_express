const express = require('express');
const bodyParser = require('body-parser');

const app = express();
// tell the app to look for static files in these directories
app.use(express.static('./server/static/'));
app.use(express.static('./client/dist/'));
app.use(bodyParser.urlencoded({ extended: false }));

// // routes
// const authRoutes = require('./server/routes/auth');
// app.use('/auth', authRoutes);

app.get('*', function(req, res, next) {
    console.log('Request: [GET]', req.originalUrl);
    res.sendFile(path.resolve(__dirname, 'index.html'));
});
// start the server
app.listen(8080, () => {
    console.log('Server is running on http://localhost:8080');
});