/**
 * Created by saria on 7/13/17.
 */
import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import routeNames from '../../routes/route-names'
//import 'bootstrap/dist/css/bootstrap.css'
class Main extends Component {
    render() {
        return (
            <div className="App">
                <div className="container">
                    <div className="col-md-6 col-md-offset-2">
                        <div className="App-header">
                            <h1>React & Redux</h1>
                            <ul>
                                <li>
                                    <Link to={routeNames.login}>Login</Link>
                                </li>
                                <li>
                                    <Link to={routeNames.register}>Register</Link>

                                </li>

                                <li>
                                    <Link to={routeNames.profile}>Profile</Link>

                                </li>
                            </ul>
                        </div>
                        <main>
                            {this.props.children}
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default Main;
