/**
 * Created by saria on 7/13/17.
 */
import React from 'react';

const UserLogin = (props) => {

    return (
        <div className="UserProfile">
            <div className="Profile-header">
                <h1>User profile</h1>
            </div>
            <div className="Profile-Panel ">

                <strong>Welcome </strong>{props.username}
            </div>
        </div>
    );

};

export default UserLogin;