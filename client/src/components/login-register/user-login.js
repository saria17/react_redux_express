/**
 * Created by saria on 7/13/17.
 */
import React, {PropTypes} from 'react';

const UserLogin = (props) => {

    return (
        <div className="UserLogin">
            <div className="Login-header">
                <h1>Login</h1>
            </div>
            <div className="Login-Panel ">

                <form onSubmit={props.onLogin}>
                    <div className="form-group">
                        <label>User name</label>
                        <input type="text" className="form-control" name="username"/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" name="password"/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-success">Login</button>
                    </div>
                </form>
            </div>
        </div>
    );

};
UserLogin.propTypes = {
    onLogin: PropTypes.func.isRequired
};
export default UserLogin;