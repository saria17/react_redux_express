/**
 * Created by saria on 7/13/17.
 */
import React, {PropTypes} from 'react';
const UserRegister = (props) => {

    return (
        <div className="UserRegister">
            <div className="Register-header">
                <h1>Register</h1>
            </div>
            <div className="Register-panel">
                <form onSubmit={props.onRegister}>
                    <div className="form-group">
                        <label>Full name</label>
                        <input type="text" className="form-control" name="username"/>
                    </div>
                    <div className="form-group">
                        <label>User name</label>
                        <input type="text" className="form-control" name="username"/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" name="password"/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-info">Register</button>
                    </div>
                </form>
            </div>
        </div>
    );

};
UserRegister.propTypes = {
    onRegister: PropTypes.func.isRequired
};
export default UserRegister;