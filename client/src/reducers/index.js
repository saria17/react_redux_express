/**
 * Created by saria on 7/13/17.
 */
import userReducer from './user-reducer.js'
import {combineReducers} from 'redux';

let reducers = combineReducers({
    userState: userReducer
});
export default reducers;