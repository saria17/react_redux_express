/**
 * Created by saria on 7/13/17.
 */
import * as types from '../actions/action-types'
const initialState = {
    user: null,
};
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.Login_User:
            return Object.assign({}, state, {user: action.user});
        case types.Register_User:
            return Object.assign({}, state, {user: action.user});

        default:
            return state;
    }
};
export default userReducer;