/**
 * Created by saria on 7/13/17.
 */
export default (
    {
        "login": "/login",
        "register": "/register",
        "home": "/home",
        "profile": "/profile"
    }
)