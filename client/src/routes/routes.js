/**
 * Created by saria on 7/13/17.
 */
import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import routeNames from '../routes/route-names'
import  Main from '../components/layouts/main'
import UserLoginContainer from '../components/login-register/user-login'
import UserRegisterContainer from '../components/login-register/user-register'
import UserProfileContainer from '../components/profile/user-profile'
export default (
    <BrowserRouter>
        <Main>
            <Route path={routeNames.login} component={UserLoginContainer}/>
            <Route path={routeNames.register} component={UserRegisterContainer}/>
            <Route path={routeNames.profile} component={UserProfileContainer}/>
        </Main>
    </BrowserRouter>
)