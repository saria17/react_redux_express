import React from 'react'
import ReactDom from 'react-dom'
import {Provider} from 'react-redux'
import router from './routes/routes'
import store from './store'
ReactDom.render(<Provider store={store}>{router}</Provider>, document.getElementById('root'));