/**
 * Created by saria on 7/13/17.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import UserProfile from "../components/profile/user-profile"
class UserProfileContainer extends Component {

    render() {
        console.log(this.props.user);
        return (
            <UserProfile user={this.props.user}/>
        );
    }
}
const mapStateToProps = store => {
    return {
        user: store.userState.user
    }
};

export default connect(mapStateToProps)(UserProfileContainer);
