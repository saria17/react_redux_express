/**
 * Created by saria on 7/13/17.
 */
import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import UserLogin from "../components/login-register/user-login";
//import * as userApi from '../../api/user-api'
class UserLoginContainer extends Component {
    handleUserLogin = (e) => {
        e.preventDefault();
        let username = e.target.username.value;
        let password = e.target.password.value;
        // userApi.loginUser(username, password);

    };

    render() {
        return (
            <UserLogin onLogin={this.handleUserLogin}/>
        );
    }
}
UserLoginContainer.propTypes = {

};
const mapStateToProps = store => {
    return {
        user: store.userState.user
    }
};

export default connect(mapStateToProps)(UserLoginContainer);